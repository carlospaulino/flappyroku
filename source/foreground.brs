Function getForeground(app) as void
    app.foregroundData = { move: MoveForeGround , speed : 1 }
   
    'load the bitmap file
    backgroundBitMap = app.bitmapset.regions["foreground"]
            
    'calculate the scaled width
    app.foregroundData.scaledBackgroundWidth = FIX(backgroundBitMap.GetWidth() * app.ScaleFactor)
    
    'calculate the scaled height
    app.foregroundData.scaleBackgroundHeight = FIX(backgroundBitMap.GetHeight() * app.ScaleFactor)

    'calculate the minimum number of repetitions to fill the screen
    app.foregroundData.repetitions = FIX(app.Screen.GetWidth() / app.foregroundData.scaledBackgroundWidth) + 2
    
    'calculate the last position. this is were we are moving the 
    app.foregroundData.lastPosition = (app.foregroundData.scaledBackgroundWidth * app.foregroundData.repetitions) - app.foregroundData.scaledBackgroundWidth
    
    
    'we are going to create an array of bitmaps that we are going to repeat 
    app.foregroundData.regions = CreateObject("roArray", app.foregroundData.repetitions, true) 
    app.foregroundData.sprites = CreateObject("roArray", app.foregroundData.repetitions, true) 

    ' create the scaled bitmap that we are going to repeat
    scaledBackground = createobject("robitmap",{width:app.foregroundData.scaledBackgroundWidth,height:app.foregroundData.scaleBackgroundHeight,alphaenable:true})
    scaledBackground.DrawScaledObject(0,0,app.ScaleFactor,app.ScaleFactor,backgroundBitMap)
    

    'calculate y position for the foreground
    app.foregroundData.y = app.screen.GetHeight() - (app.foregroundData.scaleBackgroundHeight)

    'create the first region and then we clone
    scaledBackgroundRegion = createobject("roRegion", scaledBackground, 0, 0, app.foregroundData.scaledBackgroundWidth, app.foregroundData.scaleBackgroundHeight )
    
    'Create all the new regions and sprites
    For i=1 To app.foregroundData.repetitions Step 1  
    
        ' The Region
        app.foregroundData.regions[i - 1] = scaledBackgroundRegion.copy() 
        
        ' The Sprite
        app.foregroundData.sprites[i - 1] = app.compositor.NewSprite((i - 1) * app.foregroundData.scaledBackgroundWidth, app.foregroundData.y, app.foregroundData.regions[i - 1], 81)
    End For
end function

function MoveForeGround() as void 
    For i=1 To m.repetitions Step 1
        ' Move The Sprite
        m.sprites[i - 1].MoveTo(m.sprites[i - 1].getX() - m.speed, m.y)
        
        if (m.sprites[i - 1].getX() + m.scaledBackgroundWidth) <= 0 then
            m.sprites[i - 1].MoveTo(m.lastPosition , m.y)
        end if
        
    End For
end function