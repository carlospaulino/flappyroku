Function getBackground(app)
    backgroundBitMap = app.bitmapset.regions["background"]
    
    'Let's prepare the main background
    
    scaledBackgroundWidth = FIX(backgroundBitMap.GetWidth() * app.ScaleFactor)
    
    scaleBackgroundHeight = FIX(backgroundBitMap.GetHeight() * app.ScaleFactor)

    repetitions = FIX(app.Screen.GetWidth() / scaledBackgroundWidth) + 1
    
    scaledBackground = createobject("robitmap",{width:scaledBackgroundWidth * repetitions,height:scaleBackgroundHeight,alphaenable:true})
     
    print "Background Width : " ; scaledBackgroundWidth ; " | Height : " ;  scaleBackgroundHeight ; " | Repetitions : " ; repetitions
     
    For i=1 To repetitions Step 1    
        scaledBackground.DrawScaledObject((i - 1) * scaledBackgroundWidth,0,app.ScaleFactor,app.ScaleFactor,backgroundBitMap) 
    End For
           
    scaledBackgroundRegion = createobject("roRegion", scaledBackground, 0, 0, app.Screen.GetWidth(), scaledBackground.GetHeight() )

    return scaledBackgroundRegion
end function
