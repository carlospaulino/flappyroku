Library "v30/bslDefender.brs"

Sub Main()
    app = NewGameApp()
    app.GameReset()
    
    fps = 0    
    framecount = 0
    framecounttime = 0
    timestamp = createobject("rotimespan")
    start = timestamp.totalmilliseconds()

    foreGroundX = 0
     
    ' Main Game Loop 
    while true
        ' get elapsed time since last time here
        ticks = timestamp.totalmilliseconds()              
        timestamp.mark()
        
        framecounttime = framecounttime + ticks
   
        foreGroundX = foreGroundX - 1

        app.foregrounddata.move()
        app.compositor.AnimationTick(ticks)
        app.compositor.DrawAll()
        app.screen.SwapBuffers()
         
        msg=wait(1, app.msgport)   ' 1 ms or exit if button pressed
        if type(msg)="roUniversalControlEvent" then 
            exit while
        end if   
        
        framecount = framecount + 1
 
        if framecount >= 100
            print "frames per second ="; 1000*framecount/(framecounttime+timestamp.totalmilliseconds())
            framecount = 0
            framecounttime = 0
        endif        

    end while
    
end sub

Sub appGameReset()
    m.compositor=CreateObject("roCompositor")
    
    m.compositor.SetDrawTo(m.screen, 0) ' 0 means "no background color".  Use &hFF for black. 
    
    ' The Bird
    birds = m.bitmapset.animations.bird_animation
   
    scaledBirds = CreateObject("roArray", birds.Count(), true) 
    
    scaledBirdWidth = birds[0].getWidth() * m.ScaleFactor
    scaledBirdHeight = birds[0].getHeight() * m.ScaleFactor
    
    For i=1 To birds.Count() Step 1 
       ' Create new scaled bitmap
        birdScaledBitMap = createobject("robitmap",{width:scaledBirdWidth,height:scaledBirdHeight,alphaenable:true})
        
        ' Create new scaled region
        birdScaledBitMap.DrawScaledObject(0,0,m.ScaleFactor,m.ScaleFactor,birds[i-1])
        scaledBirds[i - 1] = createobject("roRegion", birdScaledBitMap, 0, 0, scaledBirdWidth , scaledBirdHeight )
        
        ' Set the animation time        
        scaledBirds[i - 1].setTime(165)       
    End For
    
    m.birdSprite = m.compositor.NewAnimatedSprite(m.StartX,m.StartY,scaledBirds,100)
    
    ' Back ground
    m.backGround = getBackground(m)           
    m.backGroundSprite = m.compositor.NewSprite(0, 0, m.backGround, 80)
    
    ' Fore ground
    getForeground(m)
End sub

Function appGameOver() As Void
End Function


Function appEventLoop() As Void
End Function

Function newGameApp()
    app = { pipeDistance : 240 , pipeSpread : 65 } 
    
    app.screen=CreateObject("roScreen", true)  ' true := use double buffer
    if type(app.screen)<>"roScreen" then
        print "Unable to create roscreen."
        stop   ' stop exits to the debugger
    endif
    app.screen.SetAlphaEnable(true) 
    
    app.bitmapset=dfNewBitmapSet(ReadAsciiFile("pkg:/assets/spriteMap.xml"))
    if (app.bitmapset=invalid) then stop
    
    app.msgport = CreateObject("roMessagePort")
    app.screen.SetPort(app.msgport)
    app.StartX=int(app.screen.GetWidth()/3)
    app.StartY=int(app.screen.GetHeight()/2)   
    
    app.GameReset=appGameReset
    app.EventLoop=appEventLoop
    app.GameOver=appGameOver
    app.ScaleFactor = app.screen.GetHeight() /  1136
    
    return app 
End Function
