Function getPipes(app) as void
    app.pipesData = { move: MovePipes , speed : 1 , NumberOfPipes : 10 }
    
    'load the bitmap file
    pipeTopBitMap = app.bitmapset.regions["pipetop"]
    
    'load the bitmap file
    pipeBottomBitMap = app.bitmapset.regions["pipebottom"]
    
    'calculate the scaled width
    app.pipesData.scaledWidth = FIX(pipeTopBitMap.GetWidth() * app.ScaleFactor)
    
    'calculate the scaled height
    app.pipesData.scaleHeight = FIX(pipeTopBitMap.GetHeight() * app.ScaleFactor)
    
    'we are going to create an array of regions that we are going to repeat 
    app.foregroundData.topPipes = CreateObject("roArray", app.pipesData.NumberOfPipes, true) 
    app.foregroundData.bottomPipes = CreateObject("roArray", app.pipesData.NumberOfPipes, true) 
    
        
End function

function MovePipes() as void

end function